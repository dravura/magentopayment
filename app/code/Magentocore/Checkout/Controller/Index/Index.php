<?php

namespace Magentocore\Checkout\Controller\Index;
class Index extends \Magento\Checkout\Controller\Index\Index
{
    public function execute($coreRoute = null)
    {
        //$this->messageManager->addSuccess('desde el controlador checkout');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $validate = $objectManager->create('Promologistics\Sms\Model\Functions')->validateSession();
        $_url=$objectManager->create('\Magento\Framework\UrlInterface');
        $_responseFactory=$objectManager->create('\Magento\Framework\App\ResponseFactory');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
         
        // get cart items
        $items = $cart->getItems();
         
        // get custom options value of cart items
        $module_array=[];
        foreach ($items as $item) {
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
            $method_payment=$product->getPaymentVr();
            //after know the value of data
            if($method_payment && !is_null($method_payment))
            {
                $module_array[]=1;
            }
            else
            {
                $module_array[]=0;
            }
        }
        if(in_array(0,$module_array)){
            echo "No es posible pagar con este metodo de pago";
            exit();
        }
        
        return parent::execute($coreRoute);
    }
} 