define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'victor_rodriguez',
                component: 'Victor_Rodriguez/js/view/payment/method-renderer/payment'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    });
