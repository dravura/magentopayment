define([
        'jquery',
        'Magento_Payment/js/view/payment/cc-form'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Victor_Rodriguez/payment/payment'
            },

            context: function() {
                return this;
            },

            getCode: function() {
                return 'victor_rodriguez';
            },

            isActive: function() {
                return true;
            }
        });
    }
);